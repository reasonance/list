#include "List.h"

ostream& operator<<(ostream & out, const Node * n)
{
	if (n)
		n->data->write(out);
	else
		out << "Error!";

	return out;
}

List::~List()
{
	Node * current = 0;
	Node * next = first;
	while (next)
	{
		current = next;
		next = next -> next;
		delete current;
	}
}
void List::push_front(int item)
{
	Node * newItem = new Node(new DataItem<int>(item));
	this->push_front(newItem);
}
void List::push_front(double item)
{
	Node * newItem = new Node(new DataItem<double>(item));
	this->push_front(newItem);
}
void List::push_front(const char * item)
{
	Node * newItem = new Node(new DataItem<const char * >(item));
	this->push_front(newItem);
}
void List::push_front(Node * newItem)
{
	if (!first)
		last = newItem;
	else
	{
		newItem->next = first;
		first->prev = newItem;
	}

	first = newItem;
}
void List::push_back(int item)
{
	Node * newItem = new Node(new DataItem<int>(item));
	this->push_back(newItem);
}
void List::push_back(double item)
{
	Node * newItem = new Node(new DataItem<double>(item));
	this->push_back(newItem);
}
void List::push_back(const char * item)
{
	Node * newItem = new Node(new DataItem<const char * >(item));
	this->push_back(newItem);
}
void List::push_back(Node * newItem)
{
	if (last)
	{
		newItem->prev = last;
		last->next = newItem;
	}
	if (!last) first = newItem;
	last = newItem;
}
void List::print_list() const
{
	for (Node * current = first; current; current = current->next)
		cout << current << ' ';
	cout << endl;
}