CC = g++

CFLAGS = -c -Wall

all: main

main: main.o List.o
	$(CC) main.o List.o -o main

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

list.o: List.cpp
	$(CC) $(CFLAGS) List.cpp
 
clean:
	rm -rf *.o main