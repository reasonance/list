#include "List.h"

int main()
{
	List list;
	list.push_back(3);
	list.push_front("item1");
	list.push_front("item2");
	list.push_back(2.1);
	list.push_front("item3");

	list.print_list();

	return 0;
}