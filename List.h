#ifndef LIST_H
#define LIST_H

#include <iostream>
#include "Node.h"

using namespace std;

class List
{
private:
	Node * first;
	Node * last;
	friend ostream & operator<<(ostream & out, const Node * n);
public:
	List(): first(0), last(0) {}
	~List();
	void push_back(int);
	void push_back(double);
	void push_back(const char *);
	void push_back(Node*);
	void push_front(int);
	void push_front(double);
	void push_front(const char *);
	void push_front(Node*);
	void print_list() const;
};

#endif