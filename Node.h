#ifndef NODE_H
#define NODE_H

#include <iostream>
using std::ostream;

class Data
{
public:
	virtual void write(ostream & out) const = 0;
	virtual ~Data() {}
};
template<typename T>
class DataItem: public Data
{
	T data;
public:
	DataItem(T d): data(d) {}
	virtual void write(ostream & out) const;
};
class Node
{
protected:
	Data * data;
	Node * prev;
	Node * next;
public:
	friend ostream & operator<<(ostream & out, const Node * n);
	friend class List;
	Node(Data * d, Node * p = 0, Node * n = 0): data(d), prev(p), next(n) {}
	~Node() { if(data) delete data; }
};

template<typename T>
void DataItem<T>::write(ostream & out) const
{
	out << data ;
}


#endif